package fr.isima.api;

public class Cell {
    private boolean alive = false;

    private Cell() {}

    public Cell next(int total) {
        if (alive) {
            if (total < 2 || total > 3) { return createDeadCell(); }
        } else {
            if (total == 3) { return createAliveCell(); }
        }
        return this;
    }

    public boolean isAlive() {
        return alive;
    }

    public static Cell createAliveCell() {
        final Cell cell = new Cell();
        cell.alive = true;
        return cell;
    }

    public static Cell createDeadCell() {
        return new Cell();
    }
}
