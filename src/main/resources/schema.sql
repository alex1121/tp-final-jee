IF OBJECT_ID('dbo.USER', 'U') IS NOT NULL
	DROP TABLE dbo.USER;
IF OBJECT_ID('dbo.GRID', 'U') IS NOT NULL
	DROP TABLE dbo.GRID;
IF OBJECT_ID('dbo.POINT', 'U') IS NOT NULL
	DROP TABLE dbo.POINT;

CREATE TABLE USER(
	idUtilisateur INT PRIMARY KEY NOT NULL IDENTITY,
	login VARCHAR(20) NOT NULL,
	password VARCHAR(20) NOT NULL
)

CREATE TABLE GRID(
	idTable INT PRIMARY KEY NOT NULL IDENTITY,
    longeur INT NOT NULL,
	largeur INT NOT NULL
)

CREATE TABLE POINT(
	idTable INT FOREIGN KEY NOT NULL IDENTITY,
    positionX INT NOT NULL,
	positionY INT NOT NULL
)

INSERT INTO USER(idPosition,login,password)
VALUES
(0,'jeremy','jeremy'),
(1,'kevin','kevin'),
(2,'alexis','alexis');

INSERT INTO GRID(idTable, longeur, positionY)
VALUES
(0,8,8),
(1,10,10),
(2,12,12);

INSERT INTO POINT(idTable, positionX, positionY)
VALUES
(0,0,2),
(0,3,4),
(0,2,2),
(1,1,6),
(1,3,5),
(1,3,6),
(1,1,2),
(2,1,1),
(2,2,6),
(2,5,7),
(2,2,0),
(2,4,3);
