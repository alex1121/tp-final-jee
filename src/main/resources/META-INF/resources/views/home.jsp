<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>My superbe game</title>

    <script type="text/javascript"
        src="/resources/js/jquery-2.2.0.js"></script>
    <script type="text/javascript"
        src="/resources/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
        href="/resources/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css"
        href="/resources/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css"
            href="/resources/css/game.css">

    </head>
    <body>
        <div class="row">
            <div class="jumbotron col-md-8 col-md-offset-2">
                <form action="javascript: void(0);" class="form-inline">
                    <div class="input-group">
                        <input type="number" name="grid_size" />
                    </div>
                    <button type="btn" class="btn btn-success">Charger la x-ième génération</button>
                </form>
                <button type="btn" class="btn btn-info" name="play">Lancer</button>
                <button type="btn" class="btn btn-info" name="pause">Pause</button>
                <button type="btn" class="btn btn-info" name="stop">Stop</button>
            </div>
       </div>

        <table id="grid"></table>

        <footer>(Kévin Dumas, Alexis Lethuillier, Jérémy Fayolle&nbsp;&#169;)</footer>

        <script src="/resources/js/game.js"></script>
    </body>
</html>
