<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Connexion</title>
        <link type="text/css" rel="stylesheet" href="form.css" />
    </head>
    <body>
        <form method="post" action="/test">
            <fieldset>
                <legend>Connexion au jeu de le vie</legend>
                <p>Vous pouvez vous connecter via ce formulaire.</p>

                <label>Identifiant <span class="requis"></span></label>
                <input type="text" id="login" name="login"/>
                <br />

                <label">Mot de passe <span class="requis"></span></label>
                <input type="password" id="motdepasse" name="motdepasse"/>
                <br /><br />

                <input type="submit" value="Connexion"/>
                <br /><br />
            </fieldset>
        </form>
    </body>
</html>