package fr.isima.controllers;

import fr.isima.api.Grid;
import fr.isima.api.Point;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api/grids"})
public class GameApiController {

    @GetMapping("/generations/{generation}")
    public Grid generate(@PathVariable("generation") Integer generation) {
        final Grid grid = new Grid(10);
        grid.makeAlive(new Point(4, 6));
        grid.makeAlive(new Point(5, 6));
        grid.makeAlive(new Point(5, 7));
        grid.makeAlive(new Point(5, 4));
        grid.makeAlive(new Point(6, 5));
        grid.makeAlive(new Point(4, 5));
        grid.makeAlive(new Point(4, 4));
        return grid.avancerGeneration(generation);
    }



}
