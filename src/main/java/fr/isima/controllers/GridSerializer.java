package fr.isima.controllers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import fr.isima.api.Grid;
import fr.isima.api.Point;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridSerializer extends StdSerializer<Grid> {

    public GridSerializer() {
        this(null);
    }

    public GridSerializer(Class<Grid> t) {
        super(t);
    }

    @Override
    public void serialize(
            Grid value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();

        final Map<String, Object> properties = new HashMap<>();
        properties.put("size", value.getSize());

        List<Point> lp = value.getActiveCells();

        properties.put("activeCells", lp);

        jgen.writeEndObject();
    }
}