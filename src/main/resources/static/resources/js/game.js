const say = console.log;

var grille = [];
var player = {
    interval: null,
    curGen: 0
};

$(() => {       //  '() =>' <=> 'function ()'
    // page ready

    $("button[name='play']").on('click', () => {
        player.interval = setInterval(nextCall, 500);
    });

    $("button[name='pause']").on('click', () => {
        clearInterval(player.interval);
    });

    $("button[name='stop']").on('click', () => {
        clearInterval(player.interval);
        player.curGen = 0;
        apiCall(0);
    });

    $("form button").on('click', () => {
        let gen = Number.parseInt($("form input[name='grid_size']").val(), 10);

        apiCall(gen);
    });
});

function apiCall(gen){
    $.get("/api/grids/generations/" + gen, (data) => {
        updateGrid(data.size, data.activeCells);

        dispGrid();
    });
}

function nextCall() {
    apiCall(player.curGen++);
}

function dispGrid() {
    let table = $("table");
    let content = "";
    for ( let i of grille ) {
        content += "<tr>";
         for ( let j of i ) {
            content += ("<td class='" + ( j ? "alive" : "dead" ) + "'></td>");
         }
         content += "</tr>";
    }
    table.html(content);
}


function updateGrid( size, arr_data ) {
    grille = new Array(size);

    for ( let i = 0; i < size; i++ ) {
        grille[i] = new Array(size);
    }

    for ( let point of arr_data ) {
        grille[ point.x ][ point.y ] = 1;
    }
}










