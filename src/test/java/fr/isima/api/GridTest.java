package fr.isima.api;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GridTest {

    private Grid grid = new Grid(10);


    /* Pour ceux qui avaient le culôt de douter de moi... */
    @Test
    public void numberOfNeighbour_allDeadCells_shouldBe0() {
        assertThat(grid.getTotal(new Point(0, 0))).isEqualTo(0);
    }

    @Test
    public void numberOfNeighbour_1LiveCell_shouldBe1() {
        grid.makeAlive(new Point(0, 1));
        assertThat(grid.getTotal(new Point(0, 0))).isEqualTo(1);
    }

    @Test
    public void numberOfNeighbour_BorderCellSurrounded_ShouldBe3() {
        grid.makeAlive(new Point(0, 1));
        grid.makeAlive(new Point(1, 1));
        grid.makeAlive(new Point(1, 0));
        assertThat(grid.getTotal(new Point(1, 1))).isEqualTo(2);
    }


}