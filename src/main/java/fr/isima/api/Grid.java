package fr.isima.api;

import java.util.*;

import static fr.isima.api.Cell.createAliveCell;
import static fr.isima.api.Cell.createDeadCell;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class Grid {
    private HashMap<Point, Cell> grid;
    private int size;

    public Grid(int size) {
        grid = new HashMap<>();

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                grid.put(new Point(x, y), createDeadCell());
            }
        }

        this.size = size;
    }

    public Grid(Grid from) {
        grid = new HashMap<>();
        this.size = from.size;

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                grid.put(new Point(x, y), createDeadCell());
            }
        }

        List<Point> lp = from.getActiveCells();

        for ( Point p : lp ) {
            grid.put(p, createAliveCell());
        }
    }

    public Grid avancerGeneration(Integer generation) {
        Grid res = new Grid(this);

        for (int i = 0; i < generation; i++) {
            res = res.avancerGeneration();
        }

        return res;
    }

    public Grid avancerGeneration() {
        Grid res = new Grid(size);

        for (Map.Entry<Point, Cell> pair : grid.entrySet()) {
            int nbNeigh = getTotal( pair.getKey() );

            res.grid.put(pair.getKey(), pair.getValue().next(nbNeigh));
        }
        return res;
    }

    public int getTotal(Point point) {
        int total = grid.get(point).isAlive() ? -1 : 0;// -1 pour la cellule elle-meme

        int px = point.getX(), py = point.getY();

        int xmin = max(0, px - 1), xmax = min(size - 1, px + 1);
        int ymin = max(0, py - 1), ymax = min(size - 1, py + 1);

        for ( int x = xmin; x <= xmax; ++x ) {
            for ( int y = ymin; y <= ymax; ++y ) {
                if ( grid.get(new Point(x, y)).isAlive() ) {
                    total++;
                }
            }
        }
        return total;
    }

    public void makeAlive(Point point) {
        if (!this.grid.containsKey(point)) {
            throw new IllegalArgumentException(point.toString());
        }
        grid.put(point, createAliveCell());
    }

    public List<Point> getActiveCells() {
        List<Point> lp = new ArrayList<>();

        for ( Map.Entry<Point, Cell> pair : grid.entrySet() ) {
            if ( pair.getValue().isAlive() ) {
                lp.add(pair.getKey());
            }
        }

        return lp;
    }

    public int getSize() {
        return this.size;
    }
}
